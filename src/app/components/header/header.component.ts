import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ReservationService } from 'src/app/services/reservation.service';

import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from 'src/app/services/user.service';
import { SubscriptionService } from 'src/app/services/subscription.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers: [NgbDropdownConfig],
})
export class HeaderComponent implements OnInit {
  notSeenAdmin!: number;
  notSeenUser!: number;
  reservationsNotSeen!: any;
  subscriptions!: any;
  hidden!: boolean;
  isUserLogged!: boolean;
  role!: string;
  loggedUser!: string | null;
  toggleChat: boolean = true;
  toggleSingle: boolean = true;

  constructor(
    private authenticationService: AuthenticationService,
    private reservationService: ReservationService,
    private subscriptionService: SubscriptionService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isUserLogged = this.authenticationService.isUserLoggedIn();
    this.loggedUser = this.isUserLogged
      ? this.authenticationService.getUserFromLocalCache().firstName +
        ' ' +
        this.authenticationService.getUserFromLocalCache().lastName
      : null;
    this.role = this.isUserLogged
      ? this.authenticationService.getRoles()[0]
      : null;
    this.getUserHiddenNotifs();
    if (this.role == 'ADMIN') {
      this.notificationsNumberAdmin();
      this.getNotSeenReservations();
    } else {
      this.notificationsNumberUser();
      this.getSubscriptions();
    }
  }

  logout() {
    this.authenticationService.logOut();
    this.isUserLogged = false;
  }

  notificationsNumberAdmin() {
    this.reservationService
      .getNotifications()
      .subscribe((data) => (this.notSeenAdmin = data));
  }

  notificationsNumberUser() {
    this.subscriptionService
      .getNotifications()
      .subscribe((data) => (this.notSeenUser = data));
  }

  getNotSeenReservations() {
    this.reservationService.getNotSeenReservations().subscribe((data) => {
      this.reservationsNotSeen = data;
    });
  }

  getSubscriptions() {
    this.subscriptionService.getSubscriptions().subscribe((data) => {
      this.subscriptions = data;
    });
  }

  clearNotifications() {
    this.userService.changeHiddenNotifications(true).subscribe((data) => {
      this.hidden = data.notificationsHidden;
    });
  }

  getUserHiddenNotifs() {
    let userId = this.authenticationService.getUserFromLocalCache().id;
    this.userService
      .getUserByid(userId)
      .subscribe((data) => (this.hidden = data.notificationsHidden));
  }

  isSeen(reservation: any) {
    this.reservationService.changeToSeen(reservation.id).subscribe((data) => {
      reservation.seen = data.seen;
    });

    this.router.navigateByUrl('admin/allreservation');
  }

  isSeenUser(subscription: any) {
    this.subscriptionService.changeToSeen(subscription.id).subscribe((data) => {
      subscription.seen = data.seen;
    });
    this.router.navigateByUrl('menu');
  }

  // orderProd() {
  //   this.router.navigateByUrl('/products')
  // }
  //
  // seeProd() {
  //   this.router.navigateByUrl('/admin/products')
  // }
  //
  // addMenu() {
  //   this.router.navigateByUrl('/admin/add-menu')
  // }
}
