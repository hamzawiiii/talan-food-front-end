

import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Product } from 'src/app/models/Product';
import { ProductsComponent } from './products.component';

describe('ProductsComponent', () => {
  let component: ProductsComponent;
  let productService: any
  let activatedRoute : any
  let  router : any
  let products : any
  
  

  beforeEach(async () => {
    productService={
      getProducts:jest.fn(()=>of(products))
     
      },
    
      activatedRoute=jest.fn(),
    router={
      navigateByUrl : jest.fn(),
    }


    component = new ProductsComponent(productService, activatedRoute, router)


    component.ngOnInit()
   
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should showDetails',()=>{
    component.showDetails(5)
     
    expect(router.navigateByUrl).toHaveBeenCalled()
    
      

  })

  it('should ajouterProduit',()=>{
    component. ajouterProduit(5)
     
    expect(router.navigateByUrl).toHaveBeenCalled()
    
      

  })
});
