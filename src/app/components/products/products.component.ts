import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: any;

  constructor(private productService: ProductService, private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {

    this.productService.getProducts().subscribe(
      (data) => {
        this.products = data;
      }
    )
  }

  showDetails(id: number) {
    this.router.navigateByUrl(`admin/products/${id}`);
  }

  ajouterProduit(id: any) {
    this.router.navigateByUrl(`admin/addproduct/1/${id}`)
  }


}
