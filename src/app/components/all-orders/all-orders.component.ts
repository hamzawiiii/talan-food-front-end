import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {OrdersService} from 'src/app/services/orders.service';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent implements OnInit {
  @Input() idReservation!: number;
  orders!: any;

  constructor(private orderService: OrdersService, private activaterouter: ActivatedRoute, private router: Router, private authentificationservice: AuthenticationService) {
  }

  ngOnInit(): void {
    this.orderService.getOrdresByReservationId(this.idReservation).subscribe(data => {
      this.orders = data;
    });
  }

}
