import {OrdersComponent} from './orders.component';
import {of} from "rxjs";

describe('OrdersComponent', () => {
  let fixture: OrdersComponent;
  let orderServiceMock: any;
  let activaterouterMock: any;
  let routerMock: any;
  let authentificationserviceMock: any;
  let orders: any;

  beforeEach(() => {
    activaterouterMock = {
      snapshot: {
        params: jest.fn()
      }
    }
    orderServiceMock = {
      getOrdresByReservationId: jest.fn(() => of(orders))
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    fixture = new OrdersComponent(
      orderServiceMock,
      activaterouterMock,
      routerMock,
      authentificationserviceMock
    )
    fixture.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should get orders', () => {
      expect(fixture.orders).toEqual(orders);
    });
  });


  describe('Test: return', () => {
    it('should navigate to getReservationByUserId', () => {
      fixture.return()
      expect(routerMock.navigateByUrl).toBeCalledWith("/getReservationByUserId");
    });
  });
});
