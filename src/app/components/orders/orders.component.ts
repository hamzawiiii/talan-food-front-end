import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {OrdersService} from 'src/app/services/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders!: any;
  idReservation!: number;

  constructor(private orderService: OrdersService, private activaterouter: ActivatedRoute, private router: Router, private authentificationservice: AuthenticationService) {
  }

  ngOnInit(): void {
    this.idReservation = this.activaterouter.snapshot.params['id'];
    this.orderService.getOrdresByReservationId(this.activaterouter.snapshot.params['id']).subscribe(data => {
      this.orders = data;
    });


  }

  return() {
    this.router.navigateByUrl("/getReservationByUserId")
  }


}
