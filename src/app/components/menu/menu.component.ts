import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';
import { DatePipe } from '@angular/common';
import { ReservationService } from '../../services/reservation.service';
import { AuthenticationService } from '../../services/authentication.service';
import { OrdersService } from '../../services/orders.service';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import Swal from 'sweetalert2';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [DatePipe],
})
export class MenuComponent implements OnInit {
  menus: any = [];
  menu: any = {};
  salades: any = [];
  plats: any = [];
  garnitures: any = [];
  desserts: any = [];
  products: any = [];
  total!: number;
  myDate!: any;
  timeLimit!: boolean;
  permitAddProduct: boolean = false;

  constructor(
    private menuService: MenuService,
    private datePipe: DatePipe,
    private reservationService: ReservationService,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private ordersService: OrdersService,
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.timeLimit = new Date().getHours() >= 12;
    this.myDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
    this.onDateChange(this.myDate);
  }

  onDateChange(date: any) {
    this.menuService.getAllMenus().subscribe((data) => {
      this.menus = data;
      this.menu = this.menus.some(
        (product: { date: any }) => product.date == date
      )
        ? this.menus.find((object: { date: any }) => object.date == date)
        : this.menus.find((object: { date: any }) => object.date > date);
      this.salades = this.menu.listProducts.filter(
        (product: { category: any }) => product.category.id == 1
      );
      this.plats = this.menu.listProducts.filter(
        (product: { category: any }) => product.category.id == 2
      );
      this.garnitures = this.menu.listProducts.filter(
        (product: { category: any }) => product.category.id == 3
      );
      this.desserts = this.menu.listProducts.filter(
        (product: { category: any }) => product.category.id == 4
      );
    });
    this.permitAddProduct = false;
  }

  addProduct(product: any) {
    this.products.map((prod: { id: number }) =>
      prod.id == product.id
        ? this.products.splice(this.products.indexOf(prod, 0), 1)
        : false
    );
    this.products.push(product);
    this.products.map((prod: { quantity: number }) =>
      prod.quantity == 0
        ? this.products.splice(this.products.indexOf(prod, 0), 1)
        : false
    );
    this.permitAddProduct =
      this.products.length > 0 &&
      (!!this.authenticationService.getRoles()?.includes('USER') ||
        !this.authenticationService.getRoles());
  }

  confirm(date: any) {
    if (this.authenticationService.getUserFromLocalCache() == null) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer);
          toast.addEventListener('mouseleave', Swal.resumeTimer);
        },
      });
      Toast.fire({
        icon: 'info',
        title: "Vous devez d'abord vous connecter",
      }).then(() => {
        this.router.navigateByUrl('/page-login');
      });
    } else if (this.authenticationService.getUserFromLocalCache() != null) {
      this.total = this.products
        .map(
          (product: { price: number; quantity: number }) =>
            (this.total = product.price * product.quantity)
        )
        .reduce((partialSum: any, a: any) => partialSum + a, 0);
      let reservation = {
        price: this.total,
        user: this.authenticationService.getUserFromLocalCache(),
        date: date,
      };
      this.reservationService.addReservation(reservation).subscribe((data) => {
        for (const element of this.products) {
          let order = {
            reservation: data,
            product: element,
            quantity: element.quantity,
          };
          this.ordersService.addOrder(order).subscribe(() => {
            this.productService.getProduct(element.id).subscribe((product) => {
              product.quantity -= order.quantity;
              this.productService
                .updateProductQuantity(product.id, product.quantity)
                .subscribe();
            });
          });
        }
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3500,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer);
            toast.addEventListener('mouseleave', Swal.resumeTimer);
          },
        });
        Toast.fire({
          icon: 'success',
          title:
            'Votre réservation est bien confirmée, merci pour votre confiance',
        }).then(() => {
          this.router.navigateByUrl('/getReservationByUserId');
        });
        this.reservationService.confirmReservation(data).subscribe();
      });
    }
  }
}
