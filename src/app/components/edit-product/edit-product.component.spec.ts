import {EditProductComponent} from './edit-product.component';
import {FormBuilder} from "@angular/forms";
import {of} from "rxjs";
import Swal from "sweetalert2";

describe('EditProductComponent', () => {
  let fixture: EditProductComponent;
  let modalServiceMock: any;
  let activatedRouteMock: any;
  let productServiceMock: any;
  let routerMock: any;
  let formBuilderMock: any;
  let product: any;

  beforeEach(() => {
    product = {
      "id": 1,
      "name": "koseksi",
      "description": "bnine",
      "quantity": 9,
      "price": 14.5,
      "image": "image",
      "displayed": true,
      "category": {
        "id": 1,
        "name": "pp"
      }
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    productServiceMock = {
      getProduct: jest.fn(() => of(product)),
      updateProduct: jest.fn(() => of(product)),
      deleteProduct: jest.fn(() => of())
    }
    formBuilderMock = new FormBuilder()
    activatedRouteMock = {
      snapshot: {
        params: jest.fn()
      }
    }
    fixture = new EditProductComponent(
      modalServiceMock,
      activatedRouteMock,
      productServiceMock,
      routerMock,
      formBuilderMock
    )
    fixture.ngOnInit()
  });

  describe('Test: ngOnInit', () => {
    it('should initialize editProductForm', () => {
      expect(fixture.editProductForm.value.name).toEqual("koseksi");
    });
  });


  describe('Test: onFileSelected', () => {
    it('should select file', () => {
      let any = {
        target: {
          files: [new Blob()]
        }
      }
      fixture.onFileSelected(any)
      expect(fixture.selectedFile).toEqual(any.target.files[0])
    });
  });

  describe('Test: updateProduct', () => {
    it('should update product', () => {
      fixture.updateProduct()
      expect(productServiceMock.updateProduct).toBeCalled()
    });
  });

  describe('Test: deleteProduct', () => {
    it('should delete product from category 1 or 2 or 3 or 4', () => {
      fixture.deleteProduct(7)
      Swal.clickConfirm()
      setTimeout(() => {
        expect(productServiceMock.deleteProduct).toBeCalled()
      })
    });
    it('should not delete product from category 1 or 2 or 3 or 4', () => {
      fixture.deleteProduct(7)
      Swal.clickDeny()
      expect(productServiceMock.deleteProduct).not.toBeCalled()
    });
    it('should delete product from category 5', () => {
      product.category.id = 5;
      fixture.deleteProduct(7)
      Swal.clickConfirm()
      setTimeout(() => {
        expect(productServiceMock.deleteProduct).toBeCalled()
      })
    });
    it('should not delete product from category 5', () => {
      product.category.id = 5;
      fixture.deleteProduct(7)
      Swal.clickDeny()
      expect(productServiceMock.deleteProduct).not.toBeCalled()
    });
  });
});
