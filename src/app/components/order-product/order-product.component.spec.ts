import {OrderProductComponent} from './order-product.component';
import {of} from "rxjs";

describe('OrderProductComponent', () => {
  let component: OrderProductComponent;
  let datepipeMock: any;
  let productServiceMock: any;
  let activatedRouteMock: any;
  let routerMock: any;
  let reservationServiceMock: any;
  let authenticationServiceMock: any;
  let ordersServiceMock: any;
  let products: any;
  let order: any;
  let user: any;
  let reservation: any;

  beforeEach(() => {
    user = {
      email: "user@talan.com",
      firstName: "user",
      id: 17,
      lastName: "user",
      password: null,
      phone: "22222222",
      role: {id: 2, name: "USER"}
    }
    products = [
      {
        "id": 1,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 1,
          "name": "pp"
        }
      },
      {
        "id": 11,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 2,
          "name": "pp"
        }
      },
      {
        "id": 111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 3,
          "name": "pp"
        }
      },
      {
        "id": 1111,
        "name": "koseksi",
        "description": "bnine",
        "quantity": 9,
        "price": 14.5,
        "image": "image",
        "displayed": true,
        "category": {
          "id": 4,
          "name": "pp"
        }
      }
    ]
    reservation = {
      "id": 122,
      "price": 11,
      "user": {
        "id": 28,
        "firstName": "user",
        "lastName": "user",
        "email": "user@talan.com",
        "password": "$2a$10$cBjf7x2r44Xu9dJVg6U.ZuRRSszyEOVq6wMzJmQL1lJpy9/AeC.NG",
        "phone": "22222222",
        "role": {
          "id": 2,
          "name": "USER"
        }
      },
      "date": "2022-07-23",
      "confirmed": false
    }
    authenticationServiceMock = {
      getRoles: jest.fn(() => ["USER"]),
      getUserFromLocalCache: jest.fn(() => user)
    }
    datepipeMock = {
      transform: jest.fn()
    }
    productServiceMock = {
      getProducts: jest.fn(() => of(products)),
      updateQuantity: jest.fn(() => of())
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    reservationServiceMock = {
      addReservation: jest.fn(() => of(reservation)),
      confirmReservation: jest.fn(() => of())
    }
    ordersServiceMock = {
      addOrder: jest.fn(() => of())
    }
    component = new OrderProductComponent(
      datepipeMock,
      productServiceMock,
      activatedRouteMock,
      routerMock,
      reservationServiceMock,
      authenticationServiceMock,
      ordersServiceMock
    )
    component.ngOnInit()
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should initialize attributes', () => {
      expect(component.permitAddProduct).toBeTruthy();
    });
  });

  describe('Test: showDetails', () => {
    it('should navigate to product details', () => {
      component.showDetails(7)
      expect(routerMock.navigateByUrl).toBeCalledWith(`user/products/7`);
    });
  });

  describe('Test: addOrder', () => {
    it('should add order', () => {
      order = {
        "id": 11,
        "quantity": 1,
        "totalPrice": 11,
        "reste": 110
      }
      component.addOrder(order)
      expect(component.orders).toContain(order);
    });
  });

  describe('Test: changeReservationDate', () => {
    it('should change date reservation', () => {
      let any = {
        target:  {
          value: "2022-07-17"
        }
      };
      component.today = "2022-07-07"
      component.changeReservationDate(any)
      expect(component.canOrder).toBeTruthy();
    });
    it('should not order', () => {
      let any = {
        target:  {
          value: "2022-07-17"
        }
      };
      component.today = "2022-07-27"
      component.changeReservationDate(any)
      expect(component.canOrder).toBeFalsy();
    });
  });

  describe('Test: createReservation', () => {
    it('should not create reservation', () => {
      let spyInstance = jest.spyOn(authenticationServiceMock, "getUserFromLocalCache").mockReturnValue(null);
      component.createReservation()
      expect(spyInstance).toBeCalled();
      expect(reservationServiceMock.addReservation).not.toBeCalled();
    });
    it('should create reservation', () => {
      component.orders = [order]
      component.createReservation()
      expect(reservationServiceMock.addReservation).toBeCalled();
      expect(productServiceMock.updateQuantity).toBeCalled();
      expect(ordersServiceMock.addOrder).toBeCalled();
      expect(reservationServiceMock.confirmReservation).toBeCalled();
    });
  });
});
