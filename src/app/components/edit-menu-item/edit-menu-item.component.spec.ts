import {EditMenuItemComponent} from './edit-menu-item.component';
import {FormBuilder} from "@angular/forms";
import {of} from "rxjs";

describe('EditMenuItemComponent', () => {
  let fixture: EditMenuItemComponent;
  let modalServiceMock: any;
  let activatedRouteeMock: any;
  let productServiceMock: any;
  let routerMock: any;
  let formBuildereMock: any;
  let product: any;

  beforeEach(() => {
    product = {
      "id": 1,
      "name": "koseksi",
      "description": "bnine",
      "quantity": 9,
      "price": 14.5,
      "image": "image",
      "displayed": true,
      "category": {
        "id": 1,
        "name": "pp"
      }
    }
    formBuildereMock = new FormBuilder()
    activatedRouteeMock = {
      snapshot: {
        params: jest.fn()
      }
    }
    productServiceMock = {
      getProduct: jest.fn(() => of(product)),
      updateProduct: jest.fn(() => of(product))
    }
    fixture = new EditMenuItemComponent(
      modalServiceMock,
      activatedRouteeMock,
      productServiceMock,
      routerMock,
      formBuildereMock
    )
    fixture.ngOnInit()
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should initialize editProductForm', () => {
      expect(fixture.editProductForm.value.name).toEqual("koseksi");
    });
  });

  describe('Test: onFileSelected', () => {
    it('should select file', () => {
      let any = {
        target: {
          files: [new Blob()]
        }
      }
      const reader = new FileReader();
      fixture.onFileSelected(any)
      expect(fixture.imagePreview).toEqual(reader.result)
    });
  });

  describe('Test: updateProduct', () => {
    it('should update product', () => {
      fixture.updateProduct()
      expect(productServiceMock.updateProduct).toBeCalled()
    });
  });
});
