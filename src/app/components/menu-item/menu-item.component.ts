import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'src/app/models/Subscription';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SubscriptionService } from 'src/app/services/subscription.service';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.css'],
})
export class MenuItemComponent implements OnInit {
  subscribe!: boolean;
  role!: string;
  isUserLoggedIn!: boolean;
  @Input() item: any;
  @Output() newItemEvent = new EventEmitter<any>();
  product = {
    id: '',
    quantity: 0,
    price: '',
  };
  quantity: any = 0;

  constructor(
    private authenticationService: AuthenticationService,
    private subscriptionService: SubscriptionService
  ) {}

  ngOnInit(): void {
    this.getSubscriptionStatus();
    this.isUserLoggedIn = this.authenticationService.isUserLoggedIn();
    this.role = this.authenticationService.getRoles()[0];
  }

  onIncrement(id: any, price: any) {
    if (this.product.quantity < this.item.quantity) {
      this.quantity++;
    }
    this.product.id = id;
    this.product.quantity = this.quantity;
    this.product.price = price;
    this.newItemEvent.emit(this.product);
  }

  onDecrement(id: any, price: any) {
    if (this.quantity > 0) {
      this.quantity--;
      this.product.id = id;
      this.product.quantity = this.quantity;
      this.product.price = price;
      this.newItemEvent.emit(this.product);
    }
  }

  changeSubscriptionStatus() {
    let userId = this.authenticationService.getUserFromLocalCache().id;
    let subscription = { user: { id: userId }, product: { id: this.item.id } };
    this.subscriptionService
      .changeSubscriptionStatus(subscription)
      .subscribe((res) => (this.subscribe = res.activated));
  }

  getSubscriptionStatus() {
    let userId = this.authenticationService.getUserFromLocalCache().id;
    this.subscriptionService
      .getSubscriptionStatus(this.item.id, userId)
      .subscribe((res) => (this.subscribe = res));
  }
}
