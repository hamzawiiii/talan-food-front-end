import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Interactions } from 'src/app/models/Interactions';
import { InteractionService } from 'src/app/services/interaction.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-show-interaction',
  templateUrl: './show-interaction.component.html',
  styleUrls: ['./show-interaction.component.css'],
})
export class ShowInteractionComponent implements OnInit {
  constructor(
    private interactionService: InteractionService,
    private router: Router
  ) {}

  reclamations!: Interactions[];
  suggestion!: Interactions[];
  notes!: Interactions[];

  ngOnInit(): void {
    this.getAllReclamations();
    this.showSuggestions();
    this.showNotes();
  }

  public getAllReclamations(): void {
    this.interactionService.showReclamations().subscribe((data) => {
      this.reclamations = data;
    });
  }

  showSuggestions() {
    this.interactionService.showSuggestions().subscribe((data) => {
      this.suggestion = data;
    });
  }

  showNotes() {
    this.interactionService.showNotes().subscribe((data) => {
      this.notes = data;
    });
  }

  deleteInterction(id: number): void {
    this.interactionService.deleteeInteraction(id).subscribe((data) => {
      this.showSuggestions();
      this.getAllReclamations();
    });
  }

  answerClient(id: number) {
    this.router.navigate(['admin/emailresponse', id]);
  }

  answerToSuggesion(idSuggestion: number) {
    this.interactionService.answerToSuggesion(idSuggestion).subscribe();

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer);
        toast.addEventListener('mouseleave', Swal.resumeTimer);
      },
    });

    Toast.fire({
      icon: 'success',
      title: ' Reponse envoyée avec succès',
    }).then(() => {});
  }
}
