import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {of} from 'rxjs';
import {AddProductComponent} from './add-product.component';

describe('AddProductComponent', () => {
  let image: any
  let component: AddProductComponent;
  let activatedRoute: any
  let productServiceMock: any;
  let router: any;
  let formBuilder: FormBuilder
  let form: any
  let formFull: any

  beforeEach(() => {


    image = {
      target: jest.fn()
    }

    form = {
      name: '',
      description: '',
      quantity: '',
      price: '',
      displayed: 'true',
      category: '1',


    }

    formFull = {
      name: 'Toffa7',
      description: 'Bnin',
      quantity: '5',
      price: '10',
      displayed: 'true',
      category: '1'
    }


    activatedRoute = {
      snapshot: {
        params: {
          'id': 1,
          'origin': 1
        }
      }
    }
    router = {
      navigateByUrl: jest.fn()
    }
    formBuilder = new FormBuilder();
    productServiceMock = {
      addProduct: jest.fn(() => of({
          name: 'Toffa7',
          description: 'Bnin',
          quantity: '5',
          price: '10',
          displayed: 'true',
          category: '1',
        }
      ))
    }

    component = new AddProductComponent(formBuilder, productServiceMock, router, activatedRoute)
    component.ngOnInit()

  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should init', () => {

    expect(component).toBeTruthy


  })

  it('should start oninit', () => {

    component.ngOnInit()
    expect(component.addProductForm.value).toEqual(form)
    expect(component.origin).toEqual(1)
    expect(component.addId).toEqual(1)
  })
  it('should add product', () => {

    component.addProductForm.setValue({
      name: 'Toffa7',
      description: 'Bnin',
      quantity: '5',
      price: '10',
      displayed: 'true',
      category: '1'

    })
    component.addProduct()
    expect(productServiceMock.addProduct).toHaveBeenCalled

  })

  it('should return to products', () => {

    component.origin = 1
    component.return()
    expect(router.navigateByUrl).toBeCalledWith('admin/products')

  })
  it('should return to add-menu', () => {

    component.origin = 7
    component.return()
    expect(router.navigateByUrl).toBeCalledWith('admin/add-menu')

  })

  describe('Test: onFileSelected', () => {
    it('should select file', () => {
      let any = {
        target: {
          files: [new Blob()]
        }
      }
      component.onFileSelected(any)
      expect(component.selectedFile).toEqual(any.target.files[0])
    });
  });
});
