import {AddInteractionComponent} from './add-interaction.component';
import {FormBuilder} from "@angular/forms";
import {of} from "rxjs";

describe('AddInteractionComponent', () => {
  let fixture: AddInteractionComponent;
  let formBuilderMock: FormBuilder;
  let routerMock: any;
  let interactionServiceMock: any;
  let authenticationServiceMock: any;

  beforeEach(() => {
    formBuilderMock = new FormBuilder();
    const user = {
      "id": 28,
      "firstName": "user",
      "lastName": "user",
      "email": "user@talan.com",
      "password": null,
      "phone": "22222222",
      "role": {
        "id": 2,
        "name": "USER"
      }
    }
    authenticationServiceMock = {
      getUserFromLocalCache: jest.fn(() => user)
    }
    routerMock = {
      navigateByUrl: jest.fn()
    }
    interactionServiceMock = {
      addInteraction: jest.fn(() => of())
    }
    fixture = new AddInteractionComponent(
      formBuilderMock,
      routerMock,
      interactionServiceMock,
      authenticationServiceMock
    )
    fixture.ngOnInit();
  });

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('Test: ngOnInit', () => {
    it('should initialize newInteractionForm', () => {
      const newInteractionForm = {
        type: 'default',
        description: null,
        value: null,
        user: authenticationServiceMock.getUserFromLocalCache().id
      };
      expect(fixture.newInteractionForm.value).toEqual(newInteractionForm);
    });
  });

  describe('Test: addInteraction', () => {
    it('should add interaction', () => {
      fixture.addInteraction()
      let spyInstance = jest.spyOn(fixture, "return");
      expect(interactionServiceMock.addInteraction).toBeCalled();
    });
  });

  describe('Test: onIntercationChange', () => {
    it('should change type of interaction to Reclamation', () => {
      const innteraction = "Reclamation"
      fixture.onIntercationChange(innteraction)
      expect(fixture.type).toEqual("réclamation");
      expect(fixture.Reclamation).toBeTruthy();
    });
    it('should not change type of interaction to Reclamation', () => {
      const innteraction = "Reclamation "
      fixture.onIntercationChange(innteraction)
      expect(fixture.type).not.toEqual("réclamation");
      expect(fixture.Reclamation).toBeFalsy();
    });
    it('should change type of interaction to Note', () => {
      const innteraction = "Note"
      fixture.onIntercationChange(innteraction)
      expect(fixture.Note).toBeTruthy();
    });
    it('should not change type of interaction to Note', () => {
      const innteraction = "Note "
      fixture.onIntercationChange(innteraction)
      expect(fixture.Note).toBeFalsy();
    });
    it('should change type of interaction to Suggestion', () => {
      const innteraction = "Suggestion"
      fixture.onIntercationChange(innteraction)
      expect(fixture.Suggestion).toBeTruthy();
      expect(fixture.type).toEqual("proposition");
    });
    it('should not change type of interaction to Suggestion', () => {
      const innteraction = "Suggestion "
      fixture.onIntercationChange(innteraction)
      expect(fixture.Suggestion).toBeFalsy();
    });
  });

  describe('Test: returnToMenue', () => {
    it('should navigate to home', () => {
      fixture.returnToMenue()
      expect(routerMock.navigateByUrl).toBeCalledWith("")
  });
  });
});
