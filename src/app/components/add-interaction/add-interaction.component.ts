import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from 'src/app/services/authentication.service';
import {InteractionService} from 'src/app/services/interaction.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-interaction',
  templateUrl: './add-interaction.component.html',
  styleUrls: ['./add-interaction.component.css']
})
export class AddInteractionComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private router: Router, private interactionService: InteractionService, private authenticationService: AuthenticationService) {
  }

  newInteractionForm !: FormGroup;
  currentRate = 6;
  Reclamation!: boolean;
  Suggestion !: boolean;
  Note !: boolean;
  type!: string;

  ngOnInit(): void {
    this.newInteractionForm = this.formBuilder.group(
      {

        type: ['default', Validators.required],
        description: [null, Validators.required],
        value: [null],
        user: [this.authenticationService.getUserFromLocalCache().id]

      }
    );
  }

  addInteraction(): void {
    this.newInteractionForm.value.value = this.currentRate;
    this.newInteractionForm.value.user = {id: this.newInteractionForm.value.user};
    this.interactionService.addInteraction(this.newInteractionForm.value).subscribe()
    this.return()
  }

  onIntercationChange(interaction: any): void {
    if (interaction == 'Reclamation') {
      this.Reclamation = true;
      this.type = "réclamation";
    } else {
      this.Reclamation = false;
    }

    if (interaction == 'Note') {
      this.Note = true;
    } else {
      this.Note = false;
    }

    if (interaction == 'Suggestion') {
      this.Suggestion = true;
      this.type = "proposition"
    } else {
      this.Suggestion = false;
    }

  }

  return() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: ' Votre message est envoyé avec succès'
    }).then(() => {
      this.router.navigateByUrl("/menu")
    })
  }

  returnToMenue() {
    this.router.navigateByUrl("")

  }


}
