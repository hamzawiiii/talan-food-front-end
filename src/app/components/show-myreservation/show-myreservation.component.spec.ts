import {ShowMyreservationComponent} from './show-myreservation.component';
import {of} from "rxjs";

describe('ShowMyreservationComponent', () => {
  let fixture: ShowMyreservationComponent;
  let reservationServiceMock: any;
  let routerMock: any;
  let authenticationServiceMock: any;
  let datePipeMock: any;
  let user: any;
  let spyInstance: any;
  let reservation: any;

  beforeEach(() => {
    user = {
      email: "user@talan.com",
      firstName: "user",
      id: 17,
      lastName: "user",
      password: null,
      phone: "22222222",
      role: {id: 2, name: "USER"}
    }
    reservation = {
      "id": 122,
      "price": 11,
      "user": {
        "id": 17,
        "firstName": "user",
        "lastName": "user",
        "email": "user@talan.com",
        "password": "$2a$10$cBjf7x2r44Xu9dJVg6U.ZuRRSszyEOVq6wMzJmQL1lJpy9/AeC.NG",
        "phone": "22222222",
        "role": {
          "id": 2,
          "name": "USER"
        }
      },
      "date": "2022-07-23",
      "confirmed": false
    }
    reservationServiceMock = {
      getReservationByUserId: jest.fn(() => of(reservation)),
      deleteReservationById: jest.fn(() => of(true))
    }
    authenticationServiceMock = {
      getUserFromLocalCache: jest.fn(() => user)
    }
    datePipeMock = {
      transform: jest.fn()
    }
    routerMock = {
      navigate: jest.fn()
    }
    fixture = new ShowMyreservationComponent(
      reservationServiceMock,
      routerMock,
      authenticationServiceMock,
      datePipeMock
    );
    spyInstance = jest.spyOn(fixture, 'getAllResrvationByUserId');
    fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('should call getAllResrvationByUserId', () => {
      expect(spyInstance).toBeCalled();
    });
  });

  describe('Test: deleteReservationById', () => {
    it('should call deleteReservationById', () => {
      fixture.deleteReservationById(7)
      expect(reservationServiceMock.deleteReservationById).toBeCalled();
    });
  });

  describe('Test: showReservationDetails', () => {
    it('should navigate to "`/ordersreservation`"', () => {
      fixture.showReservationDetails(7)
      expect(routerMock.navigate).toBeCalledWith([`/ordersreservation`, 7]);
    });
  });

  describe('Test: annulerReservation', () => {
    it('should cancel reservation`"', () => {
      fixture.resultAnnulation = true;
      fixture.annulerReservation(7)
      expect(reservationServiceMock.deleteReservationById).toBeCalledWith(7);
    });
    it('should not cancel reservation`"', () => {
      reservationServiceMock = {
        getReservationByUserId: jest.fn(() => of(reservation)),
        deleteReservationById: jest.fn(() => of(false))
      }
      fixture = new ShowMyreservationComponent(
        reservationServiceMock,
        routerMock,
        authenticationServiceMock,
        datePipeMock
      );
      spyInstance = jest.spyOn(fixture, 'getAllResrvationByUserId');
      fixture.ngOnInit();
      fixture.annulerReservation(7)
      expect(fixture.resultAnnulation).toBeFalsy();
    });
  });
});
