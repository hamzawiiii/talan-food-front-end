import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getAllMenus(): Observable<any> {
    return this.http.get(`${this.host}/api/menus`);
  }
  public addMenu(menu:any) {
    return this.http.post<any>(`${this.host}/api/menus/menu`, menu);
  }

  // addProductToMenu(idMenu:number,id:number) {
  //   return this.httpClient.post(`${this.Url}/addproduct/${idMenu}`,id);
  // }

  public addProductToMenu(menuId:number ,prodId:number){
    return this.http.post(`${this.host}/api/menus/addproduct/${menuId}/${prodId}`,null)
  }
  
}
