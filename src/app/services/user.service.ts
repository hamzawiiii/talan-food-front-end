import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  baseUrl = 'http://localhost:8090';
  constructor(
    private httpclient: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  public changeHiddenNotifications(hidden: boolean): Observable<User> {
    let id = this.authenticationService.getUserFromLocalCache().id;
    return this.httpclient.patch<User>(
      `${this.baseUrl}/api/users/${id}`,
      hidden
    );
  }

  public getUserByid(id: number): Observable<User> {
    return this.httpclient.get<User>(`${this.baseUrl}/api/users/${id}`);
  }
}
