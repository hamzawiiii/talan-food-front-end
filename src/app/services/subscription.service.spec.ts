import { SubscriptionService } from './subscription.service';

describe('SubscriptionService', () => {
  let service: SubscriptionService;
  let httpclientSpy: any;
  let authServiceMock: any;
  let user = { id: 1 };
  let baseUrl = 'http://localhost:8090';

  beforeEach(() => {
    httpclientSpy = {
      get: jest.fn(),
      delete: jest.fn(),
      post: jest.fn(),
      patch: jest.fn(),
    };
    authServiceMock = {
      getUserFromLocalCache: jest.fn(() => user),
    };

    service = new SubscriptionService(httpclientSpy, authServiceMock);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should test getSubscriptionStatus()', () => {
    service.getSubscriptionStatus(1, 2);
    expect(httpclientSpy.get).toHaveBeenLastCalledWith(
      `${baseUrl}/api/subscriptions/1/2`
    );
  });

  it('should test changeSubscriptionStatus()', () => {
    let subscription: any;
    service.changeSubscriptionStatus(subscription);
    expect(httpclientSpy.post).toHaveBeenLastCalledWith(
      `${baseUrl}/api/subscriptions`,
      subscription
    );
  });

  it('should test getNotifications()', () => {
    service.getNotifications();
    expect(httpclientSpy.get).toHaveBeenLastCalledWith(
      `${baseUrl}/api/subscriptions/notSeen/1`
    );
  });

  it('should test getSubscriptions()', () => {
    service.getSubscriptions();
    expect(httpclientSpy.get).toHaveBeenLastCalledWith(
      `${baseUrl}/api/subscriptions/user/1`
    );
  });

  it('should test changeToSeen()', () => {
    service.changeToSeen(1);
    expect(httpclientSpy.patch).toHaveBeenLastCalledWith(
      `${baseUrl}/api/subscriptions/seen/1`,
      {}
    );
  });
});
