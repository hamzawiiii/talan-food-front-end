

import { MenuService } from './menu.service';

describe('MenuService', () => {
  let service: MenuService;
  let httpClientSpy: any

  beforeEach(() => {

    httpClientSpy = {
      get : jest.fn(),
      post : jest.fn()
    }
    service = new MenuService(httpClientSpy)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test: getAllMenus', () => {
    it('should get all menus', () => {
      service.getAllMenus()
      expect(httpClientSpy.get).toBeCalledWith(`${service.host}/api/menus`)
    });
  });

  describe('Test: addMenu', () => {
    it('should add menu', () => {
      let menu: any
      service.addMenu(menu)
      expect(httpClientSpy.post).toBeCalledWith(`${service.host}/api/menus/menu`, menu)
    });
  });

  describe('Test: addProductToMenu', () => {
    it('should add product to menu', () => {
      service.addProductToMenu(1, 7)
      expect(httpClientSpy.post).toBeCalledWith(`${service.host}/api/menus/addproduct/1/7`,null)
    });
  });
});
