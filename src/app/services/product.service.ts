import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { isQualifiedName } from 'typescript';
import { Product } from '../models/Product';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  Url = 'http://localhost:8090/api/products';

  constructor(private httpClient: HttpClient) {}

  getProducts() {
    return this.httpClient.get(`${this.Url}`);
  }

  addProduct(formData: FormData) {
    return this.httpClient.post(`${this.Url}/new/product`, formData);
  }

  updateProfileImage(formData: FormData) {
    return this.httpClient.post<any>(`${this.Url}/image`, formData);
  }

  getProduct(any: any) {
    return this.httpClient.get<Product>(`${this.Url}/product/${any}`);
  }

  getProductByCat(id: any): Observable<[]> {
    return this.httpClient.get<[]>(`${this.Url}/catid/${id}`);
  }

  updateProduct(formData: FormData) {
    return this.httpClient.post(`${this.Url}/product`, formData);
  }

  deleteProduct(id: any) {
    return this.httpClient.delete(`http://localhost:8090/api/products/${id}`);
  }
  updateQuantity(id: any, quantity: any) {
    return this.httpClient.post(
      `http://localhost:8090/api/products/quantity/${id}/${quantity}`,
      null
    );
  }

  updateProductQuantity(prodId: number, quantity: number) {
    return this.httpClient.post(
      `${this.Url}/quantity/${prodId}/${quantity}`,
      null
    );
  }

  getProductsForMenu(): Observable<[]> {
    return this.httpClient.get<[]>(`${this.Url}`);
  }

  //rating part
  getRating(id: any) {
    return this.httpClient.get(
      `http://localhost:8090/api/products/ratings/${id}/`
    );
  }

  rateProduct(any: any) {
    return this.httpClient.post(
      'http://localhost:8090/api/products/ratings',
      any
    );
  }

  rateProductByUser(idUser: any, idProduct: any) {
    return this.httpClient.get(
      `http://localhost:8090/api/products/ratings/${idUser}/${idProduct}`
    );
  }
}
