import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Subscription } from '../models/Subscription';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionService {
  baseUrl = 'http://localhost:8090';
  constructor(
    private httpclient: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  public getSubscriptionStatus(
    productId: number,
    userId: number
  ): Observable<boolean> {
    return this.httpclient.get<boolean>(
      `${this.baseUrl}/api/subscriptions/${productId}/${userId}`
    );
  }

  public changeSubscriptionStatus(subscription: any) {
    return this.httpclient.post<Subscription>(
      `${this.baseUrl}/api/subscriptions`,
      subscription
    );
  }

  public getNotifications(): Observable<number> {
    let userId = this.authenticationService.getUserFromLocalCache().id;
    return this.httpclient.get<number>(
      `${this.baseUrl}/api/subscriptions/notSeen/${userId}`
    );
  }

  public getSubscriptions(): Observable<[Subscription]> {
    let userId = this.authenticationService.getUserFromLocalCache().id;
    return this.httpclient.get<[Subscription]>(
      `${this.baseUrl}/api/subscriptions/user/${userId}`
    );
  }

  public changeToSeen(id: number): Observable<Subscription> {
    return this.httpclient.patch<Subscription>(
      `${this.baseUrl}/api/subscriptions/seen/${id}`,
      {}
    );
  }
}
