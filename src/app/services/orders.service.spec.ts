import { OrdersService } from './orders.service';

describe('OrdersService', () => {
  let service: OrdersService;
  let httpClientSpy: any;

  beforeEach(() => {
    httpClientSpy = {
      get: jest.fn(),
      post: jest.fn(),
    };
    service = new OrdersService(httpClientSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test: getOrdresByReservationId', () => {
    it('should get ordres by reservation id', () => {
      service.getOrdresByReservationId(7);
      expect(httpClientSpy.get).toBeCalledWith(`${service.Url}/reservation/7`);
    });
  });

  describe('Test: addOrder', () => {
    it('should add order', () => {
      let order: any;
      service.addOrder(order);
      expect(httpClientSpy.post).toBeCalledWith(service.Url, order);
    });
  });

  describe('Test: getOrdresByUserId', () => {
    it('should getOrdresByUserId', () => {
      let id = 1;
      service.getOrdresByUserId(id);
      expect(httpClientSpy.get).toBeCalledWith(
        `http://localhost:8090/api/ordres/user/${id}`
      );
    });
  });
  describe('Test: getOrdresByUserId', () => {
    it('should getOrdresByUserId', () => {
    
      service. getProductByPercent();
      expect(httpClientSpy.get).toBeCalledWith(
        "http://localhost:8090/api/ordres/product/quantity"
      );
    });
  });
 
});
