import {InteractionService} from './interaction.service';
import {FormControl, FormGroup} from "@angular/forms";
import {HttpParams} from "@angular/common/http";
import { of } from 'rxjs';

describe('InteractionService', () => {
  let service: InteractionService;
  let httpClientSpy: any

  beforeEach(() => {

    httpClientSpy = {
      get: jest.fn(),
      post: jest.fn(),
      delete: jest.fn()
    }
    service = new InteractionService(httpClientSpy)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test: showReclamations', () => {
    it('should show reclamations', () => {

      const rest ="hey hey"
      jest.spyOn(httpClientSpy,'get').mockReturnValue(of(rest))
      service.showReclamations()
      expect(httpClientSpy.get).toBeCalledWith(`${service.baseUrl}/api/interactions/reclamations`)
    });
  });

  describe('Test: deleteeInteraction', () => {
    it('should delete interaction', () => {
      service.deleteeInteraction(7)
      expect(httpClientSpy.delete).toBeCalledWith(`${service.baseUrl}/api/interactions/7`)
    });
  });

  describe('Test: showNotes', () => {
    it('should show notes', () => {
      service.showNotes()
      expect(httpClientSpy.get).toBeCalledWith(`${service.baseUrl}/api/interactions/notes`)
    });
  });

  describe('Test: showSuggestions', () => {
    it('should show suggestions', () => {
      service.showSuggestions()
      expect(httpClientSpy.get).toBeCalledWith(`${service.baseUrl}/api/interactions/suggestions`)
    });
  });

  describe('Test: addInteraction', () => {
    it('should add interaction', () => {
      let interaction = new FormGroup({name: new FormControl()})
      service.addInteraction(interaction)
      expect(httpClientSpy.post).toBeCalledWith(`${service.baseUrl}/api/interactions`,interaction)
    });
  });

  describe('Test: answerClient', () => {
    it('should answer client', () => {
      let params = new HttpParams();
      params = params.append('message', 'ca');
      service.answerClient(7, 'ca')
      expect(httpClientSpy.get).toBeCalledWith(`${service.baseUrl}/api/interactions/reclamation/response/7`,{params: params})
    });
  });

  describe('Test: answerToSuggesion', () => {
    it('should answer suggesion', () => {
      service.answerToSuggesion(7)
      expect(httpClientSpy.get).toBeCalledWith(`${service.baseUrl}/api/interactions/suggession/response/7`)
    });
  });
});
