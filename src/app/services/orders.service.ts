import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  Url = 'http://localhost:8090/api/ordres';
  constructor(private httpClient: HttpClient) {}

  getOrdresByReservationId(reserveId: number): Observable<any> {
    return this.httpClient.get(`${this.Url}/reservation/${reserveId}`);
  }

  addOrder(order: any) {
    return this.httpClient.post(this.Url, order);
  }

  getOrdresByUserId(id: number): Observable<any> {
    return this.httpClient.get(`${this.Url}/user/${id}`);
  }
  getProductByPercent(): Observable<any> {
    return this.httpClient.get(`${this.Url}/product/quantity`);
  }
}
