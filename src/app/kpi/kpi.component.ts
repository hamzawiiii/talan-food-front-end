import { Component, OnInit, ViewChild } from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexFill,
  ApexMarkers,
  ApexYAxis,
  ApexPlotOptions,
  ApexResponsive,
  ApexLegend,
} from 'ng-apexcharts';
import { OrdersService } from '../services/orders.service';
import { ReservationService } from '../services/reservation.service';

export type ChartOptions = {
  series?: ApexAxisChartSeries | any;
  chart?: ApexChart | any;
  xaxis?: ApexXAxis | any;
  dataLabels?: ApexDataLabels | any;
  grid?: ApexGrid | any;
  fill?: ApexFill | any;
  markers?: ApexMarkers | any;
  yaxis?: ApexYAxis | any;
  stroke?: ApexStroke | any;
  title?: ApexTitleSubtitle | any;
  plotOptions?: ApexPlotOptions | any;
  responsive?: ApexResponsive[] | any;
  labels: any;
  legend?: ApexLegend | any;
};

@Component({
  selector: 'app-kpi',
  templateUrl: './kpi.component.html',
  styleUrls: ['./kpi.component.css'],
})
export class KpiComponent implements OnInit {
  @ViewChild('chart') chart!: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  public chartOptions3: Partial<ChartOptions>;
  public chartOptionsD: Partial<ChartOptions>;
  xAxe: any[] = [];
  yAxe: any[] = [];
  xAxe2: any[] = [];
  yAxe2: any[] = [];
  xAxe3: any[] = [];
  yAxe3: any[] = [];
  xAxeD: any[] = [];
  yAxeD: any[] = [];
  constructor(
    private reservationService: ReservationService,
    private orderservice: OrdersService
  ) {
    this.chartOptions = {
      series: [
        {
          name: 'Réservations',
          data: this.yAxe,
        },
      ],
      chart: {
        height: 350,
        type: 'line',
      },
      stroke: {
        width: 7,
        curve: 'smooth',
      },
      xaxis: {
        type: 'datetime',
        categories: this.xAxe,
      },
      title: {
        text: 'Nombre de réservations par jour',
        align: 'left',
        style: {
          fontSize: '16px',
          color: '#666',
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'dark',
          gradientToColors: ['#FDD835'],
          shadeIntensity: 1,
          type: 'horizontal',
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 100, 100, 100],
        },
      },
      markers: {
        size: 4,
        colors: ['#FFA41B'],
        strokeColors: '#fff',
        strokeWidth: 2,
        hover: {
          size: 7,
        },
      },
      yaxis: {
        min: 0,
        max: 50,
        title: {
          text: 'Réservations',
        },
      },
    };

    this.chartOptions2 = {
      series: [
        {
          name: 'Réservations',
          data: this.yAxe2,
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
        innerWidth: 5,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top', // top, center, bottom
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any) {
          return val + 'Réservations';
        },
        offsetY: -5,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
      },

      xaxis: {
        categories: this.xAxe2,
        position: 'top',
        labels: {
          offsetY: -18,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
            },
          },
        },
        tooltip: {
          enabled: true,
          offsetY: -35,
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100],
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
          formatter: function (val: any) {
            return val + ' Réservations';
          },
        },
      },
      title: {
        text: `Les réservations par mois de l'année ${new Date().getFullYear()}`,
        offsetY: 330,
        align: 'center',
        style: {
          color: '#444',
        },
      },
    };

    this.chartOptions3 = {
      series: [
        {
          name: 'Réservations',
          data: this.yAxe3,
        },
      ],
      chart: {
        height: 350,
        type: 'bar',
        innerWidth: 5,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top', // top, center, bottom
          },
        },
      },
      dataLabels: {
        enabled: true,
        formatter: function (val: any) {
          return val + ' Réservations';
        },
        offsetY: -5,
        style: {
          fontSize: '12px',
          colors: ['#304758'],
        },
      },

      xaxis: {
        categories: this.xAxe3,
        position: 'top',
        labels: {
          offsetY: -18,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5,
            },
          },
        },
        tooltip: {
          enabled: true,
          offsetY: -35,
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100],
        },
      },
      yaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
          formatter: function (val: any) {
            return val + 'Réservations';
          },
        },
      },
      title: {
        text: 'Total des réservations par an',
        offsetY: 330,
        align: 'center',
        style: {
          color: '#444',
        },
      },
    };
    this.chartOptionsD = {
      series: this.yAxeD,
      chart: {
        type: 'donut',
      },
      labels:this.xAxeD,
      responsive: [
        {
          breakpoint: 575,
          options: {
            chart: {
              width: 300,
            },
            legend: {
              position: 'bottom',
              show: true,
            },
            dataLabels: {
              enabled: false,
            },
          },
        },
      ],
    };
  }

  ngOnInit(): void {
    this.getProductPercent();
    this.getPercent();
    this.getReservationByMonth();
    this.getReservationByYear();
  }
  getPercent() {
    this.reservationService
      .getAllReservationsByDateAsc()
      .subscribe((response) => {
        response.forEach((element: any) => {
          this.xAxe.push(element.date);
          //console.log(this.xAxe);

          this.yAxe.push(element.count);
          // console.log(this.yAxe);
        });
      });
  }
  getReservationByMonth() {
    this.reservationService
      .getAllReservationsByMonth()
      .subscribe((response) => {
        response.forEach((element: any) => {
          this.xAxe2.push(element.date);
          //  console.log(this.xAxe2);
          this.yAxe2.push(element.count);
          //console.log(this.yAxe2);
        });
      });
  }

  getReservationByYear() {
    this.reservationService.getAllReservationsByYear().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxe3.push(element.date);
        console.log(this.xAxe3);
        this.yAxe3.push(element.count);
        console.log(this.yAxe3);
      });
    });
  }
  getProductPercent() {
    this.orderservice.getProductByPercent().subscribe((response) => {
      response.forEach((element: any) => {
        this.xAxeD.push(element.product.name);
        console.log(this.xAxeD);
        this.yAxeD.push(element.quantity);
        console.log(this.yAxeD);
      });
    });
  }
}
