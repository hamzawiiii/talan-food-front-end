import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { StaticPriceComponent } from './static-price.component';
import { ReservationService } from '../services/reservation.service';

describe('StaticPriceComponent', () => {
  let fixture: StaticPriceComponent;
  let ReservationServiceMock: any;
  let response: HttpResponse<any>;
  beforeEach(() => {
    ReservationServiceMock = {
      getAllIncomesByDate: jest.fn(() => of(response)),
      getAllIncomesByMonth: jest.fn(() => of(response)),
      getAllIncomesByYear: jest.fn(() => of(response)),
      getAllIncomesByUser: jest.fn(() => of(response)),
    };
    fixture = new StaticPriceComponent(ReservationServiceMock);
    fixture.ngOnInit();
  });
  describe('Test: ngOnInit', () => {
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllIncomesByDate).toBeDefined();
    });
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllIncomesByMonth).toBeDefined();
    });
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllIncomesByYear).toBeDefined();
    });
    it('should call getAllIncomesByDate on init', () => {
      expect(ReservationServiceMock.getAllIncomesByUser).toBeDefined();
    });
  });


  it('should set xAxe and yAxe with data from getAllIncomesByDate', () => {
    fixture.xAxe.push('2022-01-01');
    fixture.yAxe.push(22);

    expect(fixture.xAxe).toEqual(['2022-01-01']);
    expect(fixture.yAxe).toEqual([22]);
  });

  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxe2.push('2022-01-01');
    fixture.yAxe2.push(22);

    expect(fixture.xAxe2).toEqual(['2022-01-01']);
    expect(fixture.yAxe2).toEqual([22]);
  });


  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxe3.push('2022-01-01');
    fixture.yAxe3.push(22);

    expect(fixture.xAxe3).toEqual(['2022-01-01']);
    expect(fixture.yAxe3).toEqual([22]);
  });

  it('should set xAxe and yAxe with data from  getAllIncomesByMonth', () => {
    fixture.xAxe4.push('2022-01-01');
    fixture.yAxe4.push(22);

    expect(fixture.xAxe4).toEqual(['2022-01-01']);
    expect(fixture.yAxe4).toEqual([22]);
  });


  it('should update chart when xAxe and yAxe change', () => {
    fixture.xAxe = ['2022-01-01'];
    fixture.yAxe = [22];

   expect(fixture.chartOptions.xaxis.categories).toEqual(['2022-01-01']);
   expect(fixture.chartOptions.series[0].data).toEqual([22]);
  });
});
