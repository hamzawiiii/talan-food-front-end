import { Component, OnInit, ViewChild } from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexFill,
  ApexMarkers,
  ApexYAxis,
} from 'ng-apexcharts';

export type ChartOptions = {
  series?: ApexAxisChartSeries | any;
  chart?: ApexChart | any;
  xaxis?: ApexXAxis | any;
  dataLabels?: ApexDataLabels | any;
  grid?: ApexGrid | any;
  fill?: ApexFill | any;
  markers?: ApexMarkers | any;
  yaxis?: ApexYAxis | any;
  stroke?: ApexStroke | any;
  title?: ApexTitleSubtitle | any;
};
@Component({
  selector: 'app-chart',
  templateUrl: './static.component.html',
  styleUrls: ['./chart.component.css'],
})
export class staticComponent implements OnInit {
  constructor() {}
  ngOnInit(): void {}
}
